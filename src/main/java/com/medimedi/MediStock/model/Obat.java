package com.medimedi.MediStock.model;

import java.util.List;
import java.util.ArrayList;

public class  Obat {
    
    private String nama;
    private String tipe;
    private String kegunaan;
    private List<Kadaluarsa> listKadaluarsa = new ArrayList<Kadaluarsa>();

    public Obat(String nama, String tipe, String kegunaan, Kadaluarsa kadaluarsa){
        this.nama = nama;
        this.tipe = tipe;
        this.kegunaan = kegunaan;
        this.listKadaluarsa.add(kadaluarsa);
    }
    
    public String getNama() {
        return nama;
    }

    public String getTipe() {
        return tipe;
    }

    public String getKegunaan() {
        return kegunaan;
    }
    
    public List<Kadaluarsa> getListKadaluarsa() {
        return listKadaluarsa;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public void setListKadaluarsa(List<Kadaluarsa> listKadaluarsa) {
        this.listKadaluarsa = listKadaluarsa;
    }

    public void setKegunaan(String kegunaan) {
        this.kegunaan = kegunaan;
    }

    public void addKadaluarsa(Kadaluarsa kadaluarsa) {
        listKadaluarsa.add(kadaluarsa);
    }

    public boolean equals(Obat obat) {
        if (nama.equalsIgnoreCase(obat.getNama()) &&  tipe.equalsIgnoreCase(obat.getTipe())) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isHasKadaluarsa(Kadaluarsa kadaluarsa) {
        for (Kadaluarsa kadaluarsa2 : listKadaluarsa) {
            if (kadaluarsa.isEqual(kadaluarsa2)) {
                return true;
            }
        }
        return false;
    }

}