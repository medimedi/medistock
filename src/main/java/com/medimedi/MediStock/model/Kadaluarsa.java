package com.medimedi.MediStock.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Kadaluarsa {
    
    private LocalDate tanggal;
    private int jumlah;

    public Kadaluarsa(String tanggal, int jumlah){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        this.tanggal = LocalDate.parse(tanggal, formatter);
        this.jumlah = jumlah;
    }

    public int getJumlah() {
        return jumlah;
    }

    public LocalDate getTanggal() {
        return tanggal;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public boolean cekKadaluarsa() {
        if (tanggal.isBefore(LocalDate.now())) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isEqual(Kadaluarsa kadaluarsa) {
        if (kadaluarsa.getTanggal().equals(tanggal)) {
            return true;
        } else {
            return false;
        }
    }

}