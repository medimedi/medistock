package com.medimedi.MediStock.model;

public class ObatNarkotika extends Obat {
    
    ObatNarkotika(String nama, String kegunaan, Kadaluarsa kadaluarsa){
        super(nama, "Obat Narkotika", kegunaan, kadaluarsa);
    }

}