package com.medimedi.MediStock.model;

public class ObatKeras extends Obat {
    
    ObatKeras(String nama, String kegunaan, Kadaluarsa kadaluarsa){
        super(nama, "Obat Keras", kegunaan, kadaluarsa);
    }

}