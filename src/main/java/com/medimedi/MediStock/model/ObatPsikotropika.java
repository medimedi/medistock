package com.medimedi.MediStock.model;

public class ObatPsikotropika extends Obat {
    
    ObatPsikotropika(String nama, String kegunaan, Kadaluarsa kadaluarsa){
        super(nama, "Obat Psikotropika", kegunaan, kadaluarsa);
    }

}