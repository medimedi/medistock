package com.medimedi.MediStock.model;

import java.util.List;

public class Input {

    private String nama;
    private String tipe;
    private String jumlah;
    private String kadaluarsa;
    private String kegunaan;

    Store store = new Store();

    public void setNama(String nama) {
        this.nama = nama;
    }
    
    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public void setKadaluarsa(String kadaluarsa) {
        this.kadaluarsa = kadaluarsa;
    }

    public void setKegunaan(String kegunaan) {
        this.kegunaan = kegunaan;
    }

    public String getNama() {
        return nama;
    }

    public String getTipe() {
        return tipe;
    }

    public String getJumlah() {
        return jumlah;
    }

    public String getKadaluarsa() {
        return kadaluarsa;
    }

    public String getKegunaan() {
        return kegunaan;
    }

    public static void addStock(Input input) {
        Store.addObat(input);
    }

    public static boolean updateStock(Input input) {
        Kadaluarsa kadaluarsa = new Kadaluarsa(input.getKadaluarsa(), Integer.parseInt(input.getJumlah()));
        boolean result = Store.updateObat(input.getNama(), input.getTipe(), kadaluarsa);
        return result;
    }

    public static void removeStock(Input input) {
        Store.removeObat(input.getNama(), input.getTipe());
    }

    public static List<Obat> getStock() {
        return Store.getListObat();
    }

}