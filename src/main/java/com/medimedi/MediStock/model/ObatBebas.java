package com.medimedi.MediStock.model;

public class ObatBebas extends Obat {
    
    ObatBebas(String nama, String kegunaan, Kadaluarsa kadaluarsa){
        super(nama, "Obat Bebas", kegunaan, kadaluarsa);
    }

}