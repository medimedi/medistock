package com.medimedi.MediStock.model;

import java.util.List;
import java.util.ArrayList;

public class Store {
    private static List<Obat> listObat = new ArrayList<Obat>();

    public Store(){
    }

    public static void addObat(Input input){
        String nama = input.getNama();
        String tipe = input.getTipe();
        int jumlah = Integer.parseInt(input.getJumlah());
        String tanggal = input.getKadaluarsa();
        String kegunaan = input.getKegunaan();

        Kadaluarsa kadaluarsa = new Kadaluarsa(tanggal, jumlah);
        Obat obat = new Obat(nama, tipe, kegunaan, kadaluarsa);

        listObat.add(obat);
    }

    public static boolean updateObat(String nama, String tipe, Kadaluarsa kadaluarsa) {
        if (isHas(nama, tipe)) {
            Obat obat = getObat(nama);
            if (obat.isHasKadaluarsa(kadaluarsa)) {
                for(Kadaluarsa kadaluarsaInList: obat.getListKadaluarsa()) {
                    if (kadaluarsaInList.isEqual(kadaluarsa)) {
                        kadaluarsaInList.setJumlah(kadaluarsaInList.getJumlah() + kadaluarsa.getJumlah());
                    }
                }
            } else {
                obat.addKadaluarsa(kadaluarsa);
            }
            
            return true;
        }
        return false;
        
        // for (Obat obat: listObat) {
        //     if (obat.getNama().equalsIgnoreCase(nama) && obat.getTipe().equalsIgnoreCase(tipe)) {
                
        //         return true;
        //     }
        // }
        // return false;
    }

    public static boolean removeObat(String nama, String tipe) {
        for (Obat obat: listObat) {
            if (obat.getNama().equalsIgnoreCase(nama) && obat.getTipe().equalsIgnoreCase(tipe)) {
                listObat.remove(obat);
                return true;
            }
        }
        return false;
    }

    public static List<Obat> getListObat() {
        return listObat;
    }

    public static Obat getObat(String nama){
        for(Obat obat: listObat){
            if (obat.getNama().equalsIgnoreCase(nama)){
                return obat;
            }
        }
        return null;
    }

    public static List<Obat> getTipe(String tipe){
        List <Obat> obatByTipe = new ArrayList<Obat> ();
        for(Obat obat: listObat){
            if (obat.getTipe().equalsIgnoreCase(tipe)){
                obatByTipe.add(obat);
            }
        }
        return obatByTipe;
    } 
    
    public static List<Obat> getKegunaan (String kegunaan){
        List <Obat> obatByGuna = new ArrayList<Obat> ();
        for(Obat obat: listObat){
            if (obat.getTipe().equalsIgnoreCase(kegunaan)){
                obatByGuna.add(obat);
            }
        }
        return obatByGuna;
    }

    public static boolean isHas(Obat obat) {
        for (Obat obatDiList : listObat) {
            if (obat.getNama().equals(obatDiList.getNama()) && obat.getNama().equals(obatDiList.getNama())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isHas(String nama, String tipe) {
        for (Obat obatDiList : listObat) {
            if (nama.equals(obatDiList.getNama()) && tipe.equals(obatDiList.getNama())) {
                return true;
            }
        }
        return false;
    }
   
    


}