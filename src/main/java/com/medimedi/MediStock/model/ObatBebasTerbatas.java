package com.medimedi.MediStock.model;

public class ObatBebasTerbatas extends Obat {
    
    ObatBebasTerbatas(String nama, String kegunaan, Kadaluarsa kadaluarsa){
        super(nama, "Obat Bebas Terbatas", kegunaan, kadaluarsa);
    }

}