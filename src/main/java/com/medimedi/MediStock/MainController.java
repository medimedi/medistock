package com.medimedi.MediStock;

import com.medimedi.MediStock.model.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;


@Controller
public class MainController {

    @GetMapping("/") 
        public String home(Model model) {
            return "home";
    }

    @GetMapping("/add-stock") 
        public String addStock(Model model) {
            model.addAttribute("input", new Input());
            return "add-stock";
    }

    @PostMapping("/add-stock")
    public String submitFormAddStock(@ModelAttribute Input input) {
        Input.addStock(input); 
        return "hasil-add-stock";
    }

    @GetMapping("/update-stock") 
        public String updateStock(Model model) {
            model.addAttribute("input", new Input());
            return "update-stock";
    }

    @PostMapping("/update-stock")
    public String submitFormUpdateStock(@ModelAttribute Input input) {
        Input.updateStock(input); 
        return "hasil-update-stock";
    }

    @GetMapping("/remove-stock") 
        public String removeStock(Model model) {
            model.addAttribute("input", new Input()); 
            return "remove-stock";
    }

    @PostMapping("/remove-stock")
    public String submitFormRemoveStock(@ModelAttribute Input input) {
        Input.removeStock(input); 
        return "hasil-remove-stock";
    }

    @GetMapping("/daftar-obat")
    public String daftarObat(Model model) {
        model.addAttribute("daftarObat", Store.getListObat());
        return "daftar-obat";
    }

    @GetMapping("/search-obat")
    public String searchObat (Model model) {
        return "search-obat";
    }

    @PostMapping("/search-obat")
    public String hasilSearchObat (@RequestParam String kriteria, String kataKunci, Model model) {
        if (kriteria.equalsIgnoreCase("semua")){
            model.addAttribute("daftarObat", Store.getListObat());
            return "daftar-obat";
        } else if (kriteria.equalsIgnoreCase("nama")){
            model.addAttribute("obat", Store.getObat(kataKunci));
            return "hasil-search";
        } else if (kriteria.equalsIgnoreCase("tipe")){
            model.addAttribute("daftarObat", Store.getTipe(kataKunci));
            return "daftar-obat";
        } else if (kriteria.equalsIgnoreCase("kegunaan")){
            model.addAttribute("daftarObat", Store.getKegunaan(kataKunci));
            return "daftar-obat";
        } else {
            return "search-obat";
        }
        
    }

    @GetMapping("/about")
    public String about() {
        return "about";
    }


    @GetMapping("/import_exsport")
    public String importExsportObat() {
        return "import_exsport";
    }

    @PostMapping("/hasil-import_exsport") 
    public String hasilimportExsportObat(@RequestParam("file") MultipartFile file, String kriteria,  Model model) {
        if (file.isEmpty()) {
            model.addAttribute("message", "masukkan file untuk di upload");
        }
        if (kriteria.equalsIgnoreCase("import")){
            try {
                File file2 = convert(file);
                BufferedReader br = new BufferedReader(new FileReader(file2));
                // InputStream file = multipart.getInputStream();
                // br = new BufferedReader(new InputStreamReader(file));
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (kriteria.equalsIgnoreCase("exsport")){
            try {
                File file2 = convert(file);
                PrintWriter pr = new PrintWriter(file2);
                // InputStream file = multipart.getInputStream();
                // br = new BufferedReader(new InputStreamReader(file));
            } catch (IOException e) {
                e.printStackTrace();
            }

        } 
        // try {
        // // Get the file and save it somewhere

        // } catch (IOException e) {
        //     e.printStackTrace();
        // }

        return "hasil-import_exsport";
    }
    public static File convert(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }


    
}