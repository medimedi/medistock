package com.medimedi.MediStock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MediStockApplication {

	public static void main(String[] args) {
		SpringApplication.run(MediStockApplication.class, args);
	}

}
